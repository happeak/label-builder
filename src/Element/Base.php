<?php
/**
 * Created by PhpStorm.
 * User: rustam
 * Date: 15.11.2017
 * Time: 15:05
 */

namespace HappeakApi\LabelBuilder\Element;

use  HappeakApi\LabelBuilder\Builder\Base as BaseBuilder;

abstract class Base
{
    public    $left   = 0;
    public    $top    = null;
    protected $height = null;
    protected $width = null;
    public    $params = [];


    /**
     * Base constructor.
     *
     * @param       $left
     * @param       $top
     * @param array $params
     */
    public function __construct($left, $top, $params = [])
    {
        $this->left = $left;
        $this->top = $top;
        $this->params = $params;
    }


    /**
     * @param BaseBuilder $builder
     *
     * @return mixed
     */
    abstract public function render($left, $top, BaseBuilder $builder);


    /**
     * @return int
     */
    public function width()
    {
        return $this->width;
    }


    /**
     * @return int
     */
    public function height()
    {
        return $this->height;
    }
}