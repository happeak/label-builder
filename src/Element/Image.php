<?php
/**
 * Created by PhpStorm.
 * User: rustam
 * Date: 15.11.2017
 * Time: 15:12
 */

namespace HappeakApi\LabelBuilder\Element;

use HappeakApi\LabelBuilder\Builder\Base as BaseBuilder;

class Image extends Base
{
    public function render($left, $top, BaseBuilder $builder)
    {
        list($this->width, $this->height) = $builder->image($left, $top, $this->params['path'], $this->params);
    }

}