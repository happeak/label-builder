<?php
/**
 * Created by PhpStorm.
 * User: rustam
 * Date: 15.11.2017
 * Time: 15:12
 */

namespace HappeakApi\LabelBuilder\Element;

use HappeakApi\LabelBuilder\Builder\Base as BaseBuilder;

class TextSimple extends Base
{
    protected $lines  = 1;

    public function render($left, $top, BaseBuilder $builder)
    {
        $parts = str_split(iconv('UTF-8', 'CP1251', $this->params['text']), $this->params['line-width']);
        $parts = array_map('trim', $parts);

        foreach ($parts as $line => $part)
        {
            $builder->text($left, $top + ($line * $this->params['line-height']),
                iconv('CP1251', 'UTF8', $part), $this->params);
        }

        $this->lines = count($parts);
        $this->height = count($parts) * $this->params['line-height'];
        $this->width = $this->params['light-width'];
    }

    public function lines()
    {
        return $this->lines;
    }
}