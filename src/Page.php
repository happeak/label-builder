<?php
/**
 * Created by PhpStorm.
 * User: rustam
 * Date: 15.11.2017
 * Time: 14:58
 */

namespace HappeakApi\LabelBuilder;

use HappeakApi\LabelBuilder\Element\Image;
use HappeakApi\LabelBuilder\Element\TextSimple;
use HappeakApi\LabelBuilder\Element\Line;

use HappeakApi\LabelBuilder\Builder\Base as BaseBuilder;

class Page
{
    /**
     * @var Element\Base[] $elements
     */
    protected $elements = [];
    protected $dpi      = 206;
    protected $width    = 0;
    protected $height   = 0;


    /**
     * @param int $width
     * @param int $height
     * @param int $dpi
     */
    public function __create($width, $height, $dpi = 206)
    {
        $this->width = $width;
        $this->height = $height;
        $this->dpi = $dpi;
    }


    public function addText($text, $size = 8, $left = 0, $top = null)
    {
        return $this->add(new TextSimple($left, $top, [
            'text' => $text,
            'size' => $size,
            'line-width' => 50,
            'line-height' => $this->mm2pix(10),
        ]));
    }


    public function addImage($path, $left = 0, $top = null, $width = null, $height = null)
    {
        return $this->add(new Image($left, $top, [
            'path' => $path,
            'width' => $this->mm2pix($width),
            'height' => $this->mm2pix($height),
        ]));
    }


    public function addLine($height = 1, $left = 0, $top = null)
    {
        return $this->add(new Line($left, $top, [
            'height' => $height,
        ]));
    }


    /**
     * @param Element\Base $element
     *
     * @return $this
     */
    public function add(Element\Base $element)
    {
        $this->elements[] = $element;

        return $this;
    }


    /**
     * @param BaseBuilder $builder
     *
     * @return null
     */
    public function render(BaseBuilder $builder)
    {
        $builder->start($this->width, $this->height, $this->dpi);

        $cursorTop = 0;

        foreach ($this->elements as $n => $element)
        {

            $left = $this->mm2pix($element->left);
            $top = $this->mm2pix($element->top === null ? $cursorTop : $element->top);

            $element->render($left, $top, $builder);

            $cursorTop += $this->pix2mm($element->height());
        }

        $builder->end();

        return $builder->content();
    }


    /**
     * @param float $value
     *
     * @return int
     */
    protected function mm2pix($value)
    {
        return round($value * ($this->dpi / 25.4));
    }


    /**
     * @param int $value
     *
     * @return float
     */
    protected function pix2mm($value)
    {
        return $value / $this->dpi * 25.4;
    }
}