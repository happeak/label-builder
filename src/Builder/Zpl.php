<?php
/**
 * Created by PhpStorm.
 * User: rustam
 * Date: 15.11.2017
 * Time: 16:51
 */

namespace HappeakApi\LabelBuilder\Builder;

use Zebra\Zpl\Builder as ZplBuilder;
use Zebra\Zpl\Image as ZplImage;

class Zpl extends Base
{
    protected $zpl = null;


    /**
     * @param       $width
     * @param       $height
     * @param array $params
     */
    public function start($width, $height, $params = [])
    {
        $this->zpl = new ZplBuilder();

        $this->zpl->c('WT', 'E:ARI000.FNT');
//            ->c('FT', 30, 30)
//            ->c('I28');

        $this->resource = $this->zpl->toZpl();
    }


    /**
     * @param array $params
     */
    public function end($params = [])
    {
        $this->resource = $this->zpl->toZpl();
    }


    public function text($left, $top, $value, $params = [])
    {
        $this->zpl->ft($left, $top)->a('SN', 50, 40)->fh()->fd($value)->fs();

        $this->resource = $this->zpl->toZpl();

        return [mb_strlen($value) * 40, 50];
    }


    public function image($left, $top, $value, $params = [])
    {
        $this->zpl->fo($left, $top)->gf(new ZplImage(file_get_contents($value)))->fs();
        $this->resource = $this->zpl->toZpl();

        return [$params['width'], $params['height']];
    }


    public function barcode($left, $top, $value, $params = [])
    {
        $this->resource = $this->zpl->toZpl();

        return [$params['width'], $params['height']];
    }


    public function rect($left1, $top1, $left2, $top2, $params = [])
    {
        $this->resource = $this->zpl->toZpl();

        return [$left2 - $left1, $top2 - $top1];
    }

}