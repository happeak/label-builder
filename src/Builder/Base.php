<?php
/**
 * Created by PhpStorm.
 * User: rustam
 * Date: 15.11.2017
 * Time: 16:49
 */

namespace HappeakApi\LabelBuilder\Builder;

abstract class Base
{
    protected $resource = null;

    abstract public function start($width, $height, $params = []);
    abstract public function end($params = []);

    abstract public function text($x, $y, $value, $params = []);
    abstract public function image($x, $y, $value, $params = []);
    abstract public function barcode($x, $y, $value, $params = []);
    abstract public function rect($x1, $y1, $x2, $y2, $params = []);


    /**
     * @return null
     */
    public function content()
    {
        return $this->resource;
    }
}